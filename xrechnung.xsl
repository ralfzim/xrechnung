<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:lsv="unr:lsv.de/dav8-47/postfach"
    exclude-result-prefixes="xd"
    version="1.0">
    
    <xd:doc scope="stylesheet">
        
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Jul 15, 2023</xd:p>
            <xd:p><xd:b>Author:</xd:b> ralfzimmerningkat</xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:output method="xml" indent="yes"/>
        
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match= "*[local-name()='CrossIndustryInvoice']" >
<xsl:message>Template CrossIndustrie</xsl:message>
        <lsv:MT_DAV8-47>
            <lsv:BESTELLNUMMER>
                
                <xsl:value-of select="*[local-name()='BuyerOrderReferencedDocument']/LineID"/>  
            </lsv:BESTELLNUMMER>
            <lsv:base64Data>
            <xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
                <xsl:copy-of select="."/>
                <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
            </lsv:base64Data>
            
        </lsv:MT_DAV8-47>        

    </xsl:template>
    
    <xd:doc>
        <xd:desc/>
    </xd:doc>
    <xsl:template match= "*[local-name()='Invoice']" >
        <lsv:MT_DAV8-47>
        <lsv:Invoice>
            <xsl:copy-of select="."/>
        </lsv:Invoice>
        </lsv:MT_DAV8-47>     
    </xsl:template>  

</xsl:stylesheet>